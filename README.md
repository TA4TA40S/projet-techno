# Frappasse

Dossier officiel du projet frappasse de Techno. Tous les fichiers seront aussi sauvergardés ici. 

Logiciel utilisé: 
* Piskel
* Gitkraken
* GanttProject
* LibreOffice.

> Par Aghilas, Benjamin, Pierre-Octave et Corentin.
> Fait sur Linux.